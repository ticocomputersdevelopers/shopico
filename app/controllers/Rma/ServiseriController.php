<?php

class ServiseriController extends Controller {

	public function serviseri($servis_id=0,$serviser_id=0){
		$servis = RmaOptions::user('servis');

		//servisi
		$servisi_query = DB::table('partner')->leftJoin('partner_je','partner_je.partner_id','=','partner.partner_id')->select('partner.partner_id','partner.naziv')->where('partner_je.partner_vrsta_id',118);
		if(!is_null($servis)){
			$servisi_query = $servisi_query->where('partner.partner_id',$servis->id);
		}
		$servisi = $servisi_query->orderBy('naziv','asc')->get();

		if(!is_null($servis)){
			$servis_id = $servis->id;
		}else if($servis_id <= 0 && isset($servisi[0])){
			$servis_id = $servisi[0]->partner_id;
		}

		//serviseri
		$serviseri = array();
		if($servis_id>0){
			$serviseri = DB::table('serviser')->where(array('partner_id'=>$servis_id))->get();
		}

		//serviser
		if($servis_id>0 && $serviser_id>0){
			$serviser = DB::table('serviser')->where(array('partner_id'=>$servis_id, 'serviser_id' => $serviser_id))->first();
		}else{
			$serviser = (object) array(
			  'serviser_id' => 0,
			  'ime' => '',
			  'prezime' => '',
			  'email' => '',
			  'lozinka' => '',
			  'adresa' => '',
			  'mesto' => '',
			  'telefon' => '',
			  'aktivan' => 1,
			  'partner_id' => $servis_id > 0 ? $servis_id : 0
			);
		}

        $data=array(
            "strana"=>'serviseri',
            "title"=> 'Serviseri',
            "servis_id" => $servis_id,
            "serviser_id" => $serviser_id,
            "servisi" => $servisi,
            "serviseri" => $serviseri,
            "serviser" => $serviser
        );
        return View::make('rma/pages/serviseri', $data);
	}

	public function serviseri_post(){
        $inputs = Input::get();
        
        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'email' => 'Unesite odgovarajuči mail!',
            'between' => 'Unesite više od dva a manje od 50 karaktera/brojeva!',
            'unique' => 'Mail već postoji u bazi!',
            'max'=>'Prekoračili ste limit!'
        );
        $rules = array(
			'ime' => 'required|regex:'.RmaSupport::regex().'|between:0,50',
			'prezime' => 'required|regex:'.RmaSupport::regex().'|between:0,50',
			'email' => 'email|max:50',
			'lozinka' => 'max:50',
			'adresa' => 'between:0,50',
			'mesto' => 'between:0,50',
			'telefon' => 'between:0,50'
        );
        $validator = Validator::make($inputs, $rules, $messages);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        }else{
        	$serviser_id = $inputs['serviser_id'];
        	unset($inputs['serviser_id']);

        	if($serviser_id <= 0){
                DB::table('serviser')->insert($inputs);
                AdminSupport::saveLog('RMA_SERVISER_DODAJ', array(DB::table('serviser')->max('serviser_id')));
                return Redirect::to(RmaOptions::base_url().'rma/serviseri/'.$inputs['partner_id'].'/'.DB::table('serviser')->max('serviser_id'))->with('message','Uspešno ste sačuvali izmene!');

        	}else{
        		DB::table('serviser')->where('serviser_id',$serviser_id)->update($inputs);
        		AdminSupport::saveLog('RMA_SERVISER_IZMENI', array($serviser_id));
            	return Redirect::back()->with('message','Uspešno ste sačuvali izmene!');
        	}

        }		
	}

	public function serviseri_delete($servis_id,$serviser_id){
		$serviser = DB::table('serviser')->where(array('partner_id'=>$servis_id,'serviser_id'=>$serviser_id))->first();

		if(DB::table('radni_nalog')->where('primio_serviser_id',$serviser->serviser_id)->orWhere('predao_serviser_id',$serviser->serviser_id)->count() > 0){
			return Redirect::back()->with('error_message','Servisera je nemoguće obrisati jer je vezan za radni nalog!');
		}
		AdminSupport::saveLog('RMA_SERVISER_OBRISI', array($serviser->serviser_id));
		DB::table('serviser')->where(array('serviser_id'=>$serviser->serviser_id))->delete();
		
		return Redirect::to(RmaOptions::base_url().'rma/serviseri/'.$serviser->partner_id)->with('message','Uspešno ste obrisali servisera!');
	}

}