<?php
use Export\Shopmania;

class AdminExportShopmaniaController extends Controller {

	function shopmania_grupe($kind,$key){
		return View::make('admin/export/shopmania_grupe',array('kind' => $kind,'key' => $key,'shopmania_grupe'=>Shopmania::shopmania_our_groups()));
	}

	function shopmania_grupe_povezi(){
		$kind = Input::get('kind');
		$key = Input::get('key');
		$shopmania_grupa_id = Input::get('shopmania_grupa_id');
		$grupa_pr_id = Input::get('grupa_pr_id');
        $validator = Validator::make(array('shopmania_grupa_id'=>$shopmania_grupa_id,'grupa_pr_id'=>$grupa_pr_id), array('shopmania_grupa_id' => 'required|numeric','grupa_pr_id' => 'required|numeric'),array('required'=>'Niste izabrali grupu!'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'export/shopmania-grupe/'.$kind.'/'.$key)->withInput()->withErrors($validator->messages());
        }else{
        	$grupa_pr_ids = DB::table('shopmania_grupe')->where('shopmania_grupa_id',$shopmania_grupa_id)->pluck('grupa_pr_ids');
        	if(!in_array($grupa_pr_id,Shopmania::shopmania_our_groups())){
	        	if(isset($grupa_pr_ids)){
	        		$grupa_pr_ids.=',';
	        	}
        		DB::table('shopmania_grupe')->where('shopmania_grupa_id',$shopmania_grupa_id)->update(array('grupa_pr_ids'=>$grupa_pr_ids.$grupa_pr_id));
        	}
        	return Redirect::to(AdminOptions::base_url().'export/shopmania-grupe/'.$kind.'/'.$key);
        }
		return View::make('admin/export/shopmania_grupe',array('kind' => $kind,'key' => $key));
	}

	function shopmania_grupa_dodaj(){
		$kind = Input::get('kind');
		$key = Input::get('key');
		$naziv = Input::get('naziv');
		$parent_id = Input::get('parent_id');
        $validator = Validator::make(array('naziv'=>$naziv,'parent_id'=>$parent_id), array('naziv' => 'required|regex:'.Support::regex().'|between:2,20','parent_id' => 'numeric'),array('required'=>'Popunite polje!','regex'=>'Uneli ste nedozvoljeni karakter!','between'=>'Duzina mora biti između 2 i 20 karaktera!'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'export/shopmania-grupe/'.$kind.'/'.$key)->withInput()->withErrors($validator->messages());
        }else{
        	DB::table('shopmania_grupe')->insert(array('naziv'=>$naziv,'parent_id'=>$parent_id));
        	return Redirect::to(AdminOptions::base_url().'export/shopmania-grupe/'.$kind.'/'.$key);
        }
	}

	function shopmania_grupe_obrisi(){
		$shopmania_grupa_id = Input::get('shopmania_grupa_id');
        $childs = DB::table('shopmania_grupe')->where('parent_id',$shopmania_grupa_id)->count();
		DB::table('shopmania_grupe')->where('shopmania_grupa_id',$shopmania_grupa_id)->update(array('grupa_pr_ids'=>null));
	}
	
	function shopmania_grupa_file(){
		$file = Input::file('imp_file');
		$kind = Input::get('kind');
		$key = Input::get('key');

        if(Input::hasFile('imp_file') && $file->isValid() && $file->getClientOriginalExtension() == 'ods'){
            $file->move('files/','shopmania_grupe.ods');
            $products_file = 'files/shopmania_grupe.ods';
	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        $grupe_arr = array();
	        for ($row = 1; $row <= $lastRow; $row++) {
	            $grupe_arr[] = $worksheet->getCell('A'.$row)->getValue();
	        }

	        $grupe_unq = array_unique($grupe_arr);
	        
	        

	        $all_groups=array();
	        foreach($grupe_unq as $val){
	        	$grupe = explode('>',$val);
	        	foreach($grupe as $gr){
	        		$all_groups[] = trim($gr);
	        	}
	        }

	        $db_groups = array_map('current',DB::table('shopmania_grupe')->select('naziv')->get());

	        $diff_groups = array_diff($db_groups,$all_groups);

			DB::table('shopmania_grupe')->whereIn('naziv',$diff_groups)->delete();

			foreach($grupe_unq as $value) {
				$parent_id = 0;
				$grupe = explode('>',$value);
				foreach($grupe as $grupa){
					$obj = DB::table('shopmania_grupe')->where('naziv',trim($grupa))->first();
					if(isset($obj)){
						$parent_id = $obj->shopmania_grupa_id;
					}else{
						DB::table('shopmania_grupe')->insert(array('naziv'=>trim($grupa),'parent_id'=>$parent_id));
						$parent_id = DB::table('shopmania_grupe')->max('shopmania_grupa_id');
					}

				}	
			}

        	return Redirect::to(AdminOptions::base_url().'export/shopmania-grupe/'.$kind.'/'.$key)->with('message','Fajl je uspesno učitan!');
        }else{
            return Redirect::to(AdminOptions::base_url().'export/shopmania-grupe/'.$kind.'/'.$key)->with('message','Niste izabrali fajl ili proverite strukturu fajla!');
        }
	}

}