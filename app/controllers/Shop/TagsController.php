<?php 

class TagsController extends Controller {

	public function index() {
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $tag = Request::segment(2+$offset);

		$tag_convert = $tag;

		$artikli = DB::table('roba')->select('tags')->where(array('flag_prikazi_u_cenovniku'=>1,'flag_aktivan'=>1))->get();
		foreach($artikli as $artikal){
			if(!is_null($artikal->tags) && trim($artikal->tags) != ''){
				$tags = explode(',', trim($artikal->tags));

				foreach($tags as $exists_tag){
					if(Url_mod::slugify($exists_tag)==$tag){
						$tag_convert = $exists_tag;
						break;
					}
				}
			}
		}
		
		if($tag_convert == '' || $tag_convert == null) {
			return Redirect::to(Options::base_url());
		}

		if(Session::has('limit')) {
			$limit = Session::get('limit');
		} else {
			$limit = 20;
		}

	    if(Input::get('page')) {
	    	$pageNo = Input::get('page');
	    } else {
	    	$pageNo = 1;
	    }
		$offset = ($pageNo-1) * $limit;

		$query = "SELECT roba_id,CASE WHEN r.akcija_flag_primeni = 1 AND (r.datum_akcije_od IS NULL OR r.datum_akcije_od <= '".date("Y-m-d")."') AND (r.datum_akcije_do IS NULL OR r.datum_akcije_do >= '".date("Y-m-d")."') THEN r.akcijska_cena ELSE r.".Options::checkCena()." END as custom_order_by, CASE WHEN r.roba_flag_cene_id = 11 THEN 1 ELSE 0 END as custom_order_flag, CASE WHEN (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) is null then 0 ELSE (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) END as kol FROM roba r WHERE flag_aktivan=1 AND flag_prikazi_u_cenovniku=1 AND tags ILIKE '%" . $tag_convert . "%' ";
		$query_products = DB::select($query." ORDER BY ".Options::checkCena()." ".(Options::web_options(207) == 0 ? "ASC" : "DESC")." LIMIT ".$limit." OFFSET ".$offset."");


		if(Session::has('order')){
			if(Session::get('order')=='price_asc')
			{
			$query_products = DB::select($query." ORDER BY custom_order_flag, custom_order_by ASC LIMIT ".$limit." OFFSET ".$offset."");
			}
			else if(Session::get('order')=='price_desc'){
			$query_products = DB::select($query." ORDER BY custom_order_flag, custom_order_by DESC LIMIT ".$limit." OFFSET ".$offset."");
			}
			else if(Session::get('order')=='news'){
			$query_products = DB::select($query." ORDER BY roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
			}
			else if(Session::get('order')=='name'){
			$query_products = DB::select($query." ORDER BY naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
			}
			else if(Session::get('order')=='rbr'){
			$query_products = DB::select($query." ORDER BY rbr ASC LIMIT ".$limit." OFFSET ".$offset."");
			}
			else if(Session::get('order') == 'quantity') {
			$query_products = DB::select($query." ORDER BY kol DESC LIMIT ".$limit." OFFSET ".$offset."");
			}
		}
		else {
	    	$query_products = DB::select($query." ORDER BY custom_order_flag, custom_order_by ".(Options::web_options(207) == 0 ? "ASC" : "DESC")." LIMIT ".$limit." OFFSET ".$offset."");
		}


		$data=array( 
			"strana"=>'tagovi',
			"title"=>$tag,
			"description"=>$tag.' | '.Options::company_name(),
			"keywords"=>$tag.', '.Seo::company_tag(Options::company_name()),
			"url"=>'tagovi/'.$tag,
			"articles"=>$query_products,
			"count_products"=>count(DB::select($query)),
			"filter_prikazi"=>0,
			"limit"=>$limit
		);

		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);
	}


} 