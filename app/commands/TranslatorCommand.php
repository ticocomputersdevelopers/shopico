<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Service\TranslatorService;


class TranslatorCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'translate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Translate.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$langs = $this->argument('langs');
		$langs = explode('-',$langs);
		$srKey = array_search('sr',$langs);
		if($srKey){	
			unset($langs[$srKey]);
		}
		$default_lang = DB::table('jezik')->where(array('aktivan'=>1,'izabrani'=>1))->first();
		// $defaultKey = array_search($default_lang->kod,$langs);
		// if($defaultKey){
		// 	unset($langs[$defaultKey]);
		// }

		// $filename = "files/langs/static.txt";
		// $fp = fopen($filename, "r");
		// $content = fread($fp, filesize($filename));
		// $lines = explode("\n", $content);
		// fclose($fp);

		// $filename = "files/langs/jsstatic.txt";
		// $fp = fopen($filename, "r");
		// $content = fread($fp, filesize($filename));
		// $jslines = explode("\n", $content);
		// fclose($fp);

		//default page
		foreach(DB::table('web_b2c_seo')->get() as $page){
			if(is_null(DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id'=>$page->web_b2c_seo_id,'jezik_id'=>1))->first())){
				$naziv = $page->title;
				$jezik_data = array('web_b2c_seo_id' => $page->web_b2c_seo_id, 'jezik_id' => 1, 'naziv'=> $naziv, 'slug' => $page->naziv_stranice, 'sadrzaj'=>'','title'=>$naziv,'description'=>$naziv,'keywords'=>$naziv);
                DB::table('web_b2c_seo_jezik')->insert($jezik_data); 
			}
		}

		foreach($langs as $lang){
			$jezik = DB::table('jezik')->where(array('kod'=>$lang))->first();
			$jezikPrevodilac = $jezik;
			if(is_null($jezik)){
				break;
			}
			if($jezik->jezik_id == $default_lang->jezik_id){
				$jezikPrevodilac = DB::table('jezik')->where(array('jezik_id'=>1))->first();
			}
			$translator = new TranslatorService('sr',$default_lang->kod);

			// //static
			// foreach ($lines as $line) {
			// 	if(trim($line) && trim($line) != ''){
			// 		if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezik->jezik_id,'izabrani_jezik_reci'=>trim($line)))->count() == 0){
			// 			DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezik->jezik_id,'izabrani_jezik_reci'=>trim($line),'reci'=>$translator->translate(trim($line))));
			// 		}
			// 	}
			// }
			// //jsstatic
			// foreach ($jslines as $jsline) {
			// 	if(trim($jsline) && trim($jsline) != ''){
			// 		if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezik->jezik_id,'izabrani_jezik_reci'=>trim($jsline)))->count() == 0){
			// 			DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezik->jezik_id,'izabrani_jezik_reci'=>trim($jsline),'reci'=>$translator->translate(trim($jsline)),'is_js'=>1));
			// 		}
			// 	}
			// }

			//pages
			foreach(DB::table('web_b2c_seo')->get() as $page){
				$page_lang = DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id'=>$page->web_b2c_seo_id,'jezik_id'=>$jezik->jezik_id))->first();
				if(is_null($page_lang)){
					$page_deff = DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id'=>$page->web_b2c_seo_id,'jezik_id'=>1))->first();

					$naziv = $translator->translate($page->title);
					$jezik_data = array('web_b2c_seo_id' => $page->web_b2c_seo_id, 'jezik_id' => $jezik->jezik_id, 'naziv'=> $naziv, 'slug' => AdminOptions::slugify($naziv), 'sadrzaj'=>$translator->translate(html_entity_decode($page_deff->sadrzaj)),'title'=>$translator->translate($page_deff->title),'description'=>$translator->translate($page_deff->description),'keywords'=>$translator->translate($page_deff->keywords));
                    DB::table('web_b2c_seo_jezik')->insert($jezik_data); 
				}
			}

			//front labele
			foreach(DB::table('web_b2c_seo')->select('seo_title')->whereIn('naziv_stranice',array('pocetna','akcija','blog'))->get() as $front_labele){
				$front_labele_seo_title = $translator->translate($front_labele->seo_title);
				if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'izabrani_jezik_reci'=>$front_labele_seo_title))->count() == 0 && $jezikPrevodilac->jezik_id!=$default_lang->jezik_id){
					DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'reci'=>$front_labele->seo_title,'izabrani_jezik_reci'=>$front_labele_seo_title));
				}
			}

			//banners
			foreach(DB::table('baneri')->get() as $banner){
				$banner_lang = DB::table('baneri_jezik')->where(array('baneri_id'=>$banner->baneri_id,'jezik_id'=>$jezik->jezik_id))->first();
				if(is_null($banner_lang)){
					$banner_deff = DB::table('baneri_jezik')->where(array('baneri_id'=>$banner->baneri_id,'jezik_id'=>1))->first();
					if(!is_null($banner_deff)){
						$jezik_data = array('baneri_id' => $banner->baneri_id, 'jezik_id' => $jezik->jezik_id, 'naslov'=> $translator->translate($banner_deff->naslov), 'nadnaslov'=> $translator->translate($banner_deff->nadnaslov), 'sadrzaj'=>$translator->translate(html_entity_decode($banner_deff->sadrzaj)),'naslov_dugme'=>$translator->translate($banner_deff->naslov_dugme));
	                    DB::table('baneri_jezik')->insert($jezik_data);
	                }
				}
			}

			//blog
			foreach(DB::table('web_vest_b2c')->get() as $blog){
				$blog_lang = DB::table('web_vest_b2c_jezik')->where(array('web_vest_b2c_id'=>$blog->web_vest_b2c_id,'jezik_id'=>$jezik->jezik_id))->first();
				if(is_null($blog_lang)){
					$blog_deff = DB::table('web_vest_b2c_jezik')->where(array('web_vest_b2c_id'=>$blog->web_vest_b2c_id,'jezik_id'=>1))->first();
					if(!is_null($blog_deff)){
						$jezik_data = array('web_vest_b2c_id' => $blog->web_vest_b2c_id, 'jezik_id' => $jezik->jezik_id, 'naslov'=> $translator->translate($blog_deff->naslov), 'sadrzaj'=>$translator->translate(html_entity_decode($blog_deff->sadrzaj)),'title'=>$translator->translate($blog_deff->title),'description'=>$translator->translate($blog_deff->description),'keywords'=>$translator->translate($blog_deff->keywords));
	                    DB::table('web_vest_b2c_jezik')->insert($jezik_data);
	                }
				}
			}

			//footer
			foreach(DB::table('futer_sekcija')->get() as $footer){
				$footer_lang = DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$footer->futer_sekcija_id,'jezik_id'=>$jezik->jezik_id))->first();
				if(is_null($footer_lang)){
					$footer_deff = DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$footer->futer_sekcija_id,'jezik_id'=>1))->first();
					if(!is_null($footer_deff)){
						$jezik_data = array('futer_sekcija_id' => $footer->futer_sekcija_id, 'jezik_id' => $jezik->jezik_id, 'naslov'=> $translator->translate($footer_deff->naslov), 'sadrzaj'=>$translator->translate(html_entity_decode($footer_deff->sadrzaj)));
	                    DB::table('futer_sekcija_jezik')->insert($jezik_data);
	                }
				}
			}

			//front admin labele
			foreach(DB::table('front_admin_labele')->get() as $front_admin_labele){
				$front_admin_labele_labela = $translator->translate($front_admin_labele->labela);
				if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'izabrani_jezik_reci'=>$front_admin_labele_labela))->count() == 0 && $jezikPrevodilac->jezik_id!=$default_lang->jezik_id){
					DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'reci'=>$front_admin_labele->labela,'izabrani_jezik_reci'=>$front_admin_labele_labela));
				}
			}

			//front admin
			foreach(DB::table('front_admin_labele_jezik')->where('jezik_id',1)->get() as $front_admin){
				$front_admin_lang = DB::table('front_admin_labele_jezik')->where(array('front_admin_labele_id'=>$front_admin->front_admin_labele_id,'jezik_id'=>$jezik->jezik_id))->first();
				if(is_null($front_admin_lang)){
					$jezik_data = array('front_admin_labele_id' => $front_admin->front_admin_labele_id, 'jezik_id' => $jezik->jezik_id, 'sadrzaj'=>$translator->translate(html_entity_decode($front_admin->sadrzaj)));
                    DB::table('front_admin_labele_jezik')->insert($jezik_data);
				}
			}

			//newslatter
			foreach(DB::table('web_b2c_newslatter_description')->where('jezik_id',1)->get() as $newslatter){
				$newslatter_lang = DB::table('web_b2c_newslatter_description')->where(array('jezik_id'=>$jezik->jezik_id))->first();
				if(is_null($newslatter_lang)){
					$web_b2c_newslatter_description_id = DB::table('web_b2c_newslatter_description')->max('web_b2c_newslatter_description_id')+1;
					$jezik_data = array('web_b2c_newslatter_description_id' => $web_b2c_newslatter_description_id, 'jezik_id' => $jezik->jezik_id, 'naslov'=>$translator->translate($newslatter->naslov), 'sadrzaj'=>$translator->translate(html_entity_decode($newslatter->sadrzaj)));
                    DB::table('web_b2c_newslatter_description')->insert($jezik_data);
				}
			}

			//validator jezik
			foreach(DB::table('validator_jezik')->where('jezik_id',1)->get() as $front_admin){
				$front_admin_lang = DB::table('validator_jezik')->where(array('validator_id'=>$front_admin->validator_id,'jezik_id'=>$jezik->jezik_id))->first();
				if(is_null($front_admin_lang)){
					$jezik_data = array('validator_id' => $front_admin->validator_id, 'jezik_id' => $jezik->jezik_id, 'poruka'=>$translator->translate($front_admin->poruka));
                    DB::table('validator_jezik')->insert($jezik_data);
				}
			}

			//group
			foreach(DB::table('grupa_pr')->get() as $grupa_pr){
				$grupa_pr_grupa = $translator->translate($grupa_pr->grupa);
				if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'izabrani_jezik_reci'=>$grupa_pr_grupa))->count() == 0 && $jezikPrevodilac->jezik_id!=$default_lang->jezik_id){
					DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'reci'=>$grupa_pr->grupa,'izabrani_jezik_reci'=>$grupa_pr_grupa));
				}

				$grupa_pr_lang = DB::table('grupa_pr_jezik')->where(array('grupa_pr_id'=>$grupa_pr->grupa_pr_id,'jezik_id'=>$jezik->jezik_id))->first();
				if(is_null($grupa_pr_lang)){
					$grupa_pr_deff = DB::table('grupa_pr_jezik')->where(array('grupa_pr_id'=>$grupa_pr->grupa_pr_id,'jezik_id'=>1))->first();
					if(!is_null($grupa_pr_deff)){
						$jezik_data = array('grupa_pr_id' => $grupa_pr->grupa_pr_id, 'jezik_id' => $jezik->jezik_id, 'title'=> $translator->translate($grupa_pr_deff->title), 'description'=> $translator->translate($grupa_pr_deff->description), 'keywords'=> $translator->translate($grupa_pr_deff->keywords), 'sablon_opis'=>$translator->translate(html_entity_decode($grupa_pr_deff->sablon_opis)));
	                    DB::table('grupa_pr_jezik')->insert($jezik_data);
	                }
				}
			}

			//type
			foreach(DB::table('tip_artikla')->where('tip_artikla_id','>',-1)->whereNotNull('naziv')->where('naziv','!=','')->get() as $tip_artikla){
				$tip_artikla_naziv = $translator->translate($tip_artikla->naziv);
				if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'izabrani_jezik_reci'=>$tip_artikla_naziv))->count() == 0 && $jezikPrevodilac->jezik_id!=$default_lang->jezik_id){
					DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'reci'=>$tip_artikla->naziv,'izabrani_jezik_reci'=>$tip_artikla_naziv));
				}
			}

			//tags
			foreach(DB::table('roba')->select('tags')->whereNotNull('naziv')->where('naziv','!=','')->get() as $roba){
				foreach(explode(',',$roba->tags) as $tag){
					$tag = trim($tag);
					$tagTr = $translator->translate($tag);
					if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'izabrani_jezik_reci'=>$tagTr))->count() == 0 && $jezikPrevodilac->jezik_id!=$default_lang->jezik_id){
						DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'reci'=>$tag,'izabrani_jezik_reci'=>$tagTr));
					}
				}
			}

			//article state
			foreach(DB::table('roba_flag_cene')->where('roba_flag_cene_id','>',-1)->whereNotNull('naziv')->where('naziv','!=','')->get() as $roba_flag_cene){
				$roba_flag_cene_naziv = $translator->translate($roba_flag_cene->naziv);
				if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'izabrani_jezik_reci'=>$roba_flag_cene_naziv))->count() == 0 && $jezikPrevodilac->jezik_id!=$default_lang->jezik_id){
					DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'reci'=>$roba_flag_cene->naziv,'izabrani_jezik_reci'=>$roba_flag_cene_naziv));
				}
			}

			//nacin isporuke
			foreach(DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id','>',-1)->whereNotNull('naziv')->where('naziv','!=','')->get() as $nacin_isporuke){
				$nacin_isporuke_naziv = $translator->translate($nacin_isporuke->naziv);
				if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'izabrani_jezik_reci'=>$nacin_isporuke_naziv))->count() == 0 && $jezikPrevodilac->jezik_id!=$default_lang->jezik_id){
					DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'reci'=>$nacin_isporuke->naziv,'izabrani_jezik_reci'=>$nacin_isporuke_naziv));
				}
			}

			//nacin isporuke
			foreach(DB::table('web_nacin_placanja')->where('web_nacin_placanja_id','>',-1)->whereNotNull('naziv')->where('naziv','!=','')->get() as $nacin_placanja){
				$nacin_placanja_naziv = $translator->translate($nacin_placanja->naziv);
				if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'izabrani_jezik_reci'=>$nacin_placanja_naziv))->count() == 0 && $jezikPrevodilac->jezik_id!=$default_lang->jezik_id){
					DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$default_lang->jezik_id,'jezik_id'=>$jezikPrevodilac->jezik_id,'reci'=>$nacin_placanja->naziv,'izabrani_jezik_reci'=>$nacin_placanja_naziv));
				}
			}


		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('langs', InputArgument::OPTIONAL, 'Languages.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}