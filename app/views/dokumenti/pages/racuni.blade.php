@extends('dokumenti.templates.main')
@section('content')

<div id="main-content" class="kupci-page">

	<div class="row"> 
		<div class="columns medium-3">  
			<form method="GET" action="{{DokumentiOptions::base_url()}}dokumenti/racuni" class="columns medium-10 no-padd">
				<div class="m-input-and-button">
					<input type="text" name="search" value="{{ urldecode($search) }}" placeholder="{{ AdminLanguage::transAdmin('Pretraga') }}..." class="m-input-and-button__input">
					<input class="btn btn-primary m-input-and-button__btn" value="Pretraga" type="submit">
					<a class="btn btn-danger btn-small" href="{{DokumentiOptions::base_url()}}dokumenti/racuni">{{ AdminLanguage::transAdmin('Poništi') }}</a>
				</div> 
			</form>
		</div>
		<div class="columns medium-4">
			<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="ponuda"> Ponude
			<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="predracun"> Predracuni
			<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="racun" {{($vrsta_dokumenta == 'racun') ? 'checked' : ''}}> Racuni
			<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="avansni_racun" {{($vrsta_dokumenta == 'avansni_racun') ? 'checked' : ''}}> Avansni racuni 
		</div>
		<div class="columns medium-2">
			<div class="m-input-and-button">
				<h2>Status</h2>
				<select id="JSStatusSearch">
					<option value="all">Svi statusi</option>
					{{ Dokumenti::statusSelect($dokumenti_status_id) }}
				</select>
			</div> 
		</div>
		<div class="columns medium-2">
			<div class="m-input-and-button">
				<h2>Kupac</h2>
				<select id="JSParnerSearch">
					<option value="0">Svi kupci</option>
					{{ Dokumenti::partnerSelect($partner_id) }}
				</select>
			</div> 
		</div>
		<div class="columns medium-1">
			<a href="{{DokumentiOptions::base_url()}}dokumenti/racun/0" class="btn btn-primary m-input-and-button__btn">{{ AdminLanguage::transAdmin('Novi dokument') }}</a>
		</div>
	</div>

 	<div class="row"> 
		<div class="columns medium-12 large-12">
			<label>{{ AdminLanguage::transAdmin('Ukupno') }}: {{ $count }}</label>
			<table>
				<tr>
					<th class="JSSort" data-sort_column="broj_dokumenta" data-sort_direction="{{ $sort_column == 'broj_dokumenta' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Broj Dokumenta') }}</th>
					<th class="JSSort" data-sort_column="naziv_partnera" data-sort_direction="{{ $sort_column == 'naziv_partnera' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Kupac') }}</th>
					<th class="JSSort" data-sort_column="datum_racuna" data-sort_direction="{{ $sort_column == 'datum_racuna' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Datum izdavanja') }}</th>
					<th class="JSSort" data-sort_column="rok_placanja" data-sort_direction="{{ $sort_column == 'rok_placanja' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Rok za plaćanje') }}</th>
					<th class="JSSort" data-sort_column="iznos" data-sort_direction="{{ $sort_column == 'iznos' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Ukupno') }} (din.)</th>
					<th></th>
				</tr>
				@foreach($racuni as $row)
				<tr>
					<td>{{ $row->broj_dokumenta }}</td>
					<td>{{ $row->naziv_partnera }}</td>
					<td>{{ $row->datum_racuna }}</td>
					<td>{{ $row->rok_placanja }}</td>
					<td>{{ number_format($row->iznos,2,",",".") }}</td>
					<td><a href="{{DokumentiOptions::base_url()}}dokumenti/racun/{{$row->racun_id}}">{{ AdminLanguage::transAdmin('Izmeni') }}</a></td>
				</tr>
				@endforeach

			</table>
			{{ Paginator::make($racuni,$count,$limit)->links() }}
		</div>
	</div>
</div>
@endsection