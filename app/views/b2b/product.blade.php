@extends('b2b.layouts.b2bpage')
@section('head')
    <link href="{{ B2bOptions::base_url()}}css/slick.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <?php B2bCommon::articleViewB2B($roba_id); ?>
    <ul class="breadcrumbs clearfix">
        {{ B2bArticle::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
    </ul>
    <section id="product-preview" itemscope itemtype="http://schema.org/Product" class="clearfix">

        <div class="row">

            <!-- PRODUCT PREVIEW IMAGE -->

            <section class="product-preview-image medium-6 columns">

                <a class="fancybox" rel="gallery_01" href="{{ B2bOptions::base_url() }}{{ B2bArticle::web_slika_big($roba_id) }}">
							    <span id="zoom_03" class="zoom_03" style="display: block;" data-zoom-image="{{ B2bOptions::base_url() }}{{ B2bArticle::web_slika_big($roba_id) }}" >
								<img  itemprop="image" class="zoom_03" src="{{ B2bOptions::base_url() }}{{ B2bArticle::web_slika_big($roba_id) }}" alt="{{ B2bArticle::seo_title($roba_id)}}"/>
								</span>
                </a>

                <div id="gallery_01" class="clearfix">

                    {{ B2bArticle::get_list_images($roba_id) }}

                </div>

            </section>

            <!-- PRODUCT PREVIEW INFO -->
                    <?php
                    if(B2bOptions::vodjenje_lagera()==1)
                    {$dost_kol = B2bArticle::quantityB2b($roba_id) - B2bBasket::getB2bQuantityItem($roba_id);}
                    else {
                    $dost_kol=1;
                    } ?>

            <section class="product-preview-info medium-6 columns">

                <h1 class="article-heading" itemprop="name">{{ B2bArticle::seo_title($roba_id)}}</h1>

                <ul>
                    <li class="product-group">Proizvod iz grupe:{{ B2bArticle::get_grupaB2b($roba_id) }}</li>
                    <li class="product-manufacturer" itemprop="brand">Proizvodjač:{{ B2bArticle::get_proizvodjac($roba_id) }}</li>
                    @if(B2bOptions::vodjenje_lagera() == 1)
                        <li class="product-available-amount">Dostupna količina: <a href="#"> {{$dost_kol}}</a></li>
                    @endif
                </ul>

                <section class="product-preview-price">

                    <span class="product-preview-price-new" itemprop="price">{{ B2bBasket::cena(B2bArticle::b2bRabatCene($roba_id)->ukupna_cena) }}</span>
                </section>

                <section class="add-to-cart-area clearfix">
                    @if(B2bArticle::getStatusArticle($roba_id) == 1)
                        @if($dost_kol>0)
                        <button class="add-to-cart add-to-cart-btn" data-roba-id="{{$roba_id}}">Dodaj u korpu</button>
                        <!-- <a class="add-amount-less"  href="javascript:void(0)"><</a> -->
                        <input class="add-amount" data-max-quantity="{{$dost_kol}}" type="text" value="1">
                        <!-- <a class="add-amount-more" data-max-quantity="{{$dost_kol}}" href="javascript:void(0)">></a> -->
                        @else
                            <button class="add-to-cart" >Nije dostupno</button>
                        @endif
                    @else
                    <button class="add-to-cart" >{{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($roba_id),'naziv') }}</button>
                    @endif
                </section>
				<section class="facebook-btn-share">
					<div class="fb-share-button" data-href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($roba_id))}}" data-layout="button"></div>
				</section>

            </section>

        </div>

        <!-- PRODUCT PREVIEW TABS-->

        <section class="product-preview-tabs">

            <ul class="tab-titles clearfix">

                <li class="tab-title-description active">Opis</li>
                <!-- <li class="tab-title-tehnicaldoc">Tehnička dokumentacija</li> -->
                <!-- <li class="tab-title-comments">Komentari</li> -->

            </ul>

            <section class="tab-content description-content active clearfix" >

                <p itemprop="description">{{ B2bArticle::get_opis($roba_id) }}</p>

                {{ B2bArticle::get_karakteristike($roba_id) }}

            </section>

            <section class="tab-content tehnicaldoc-content">
                <p>proba </p>
            </section>

            <section class="tab-content comments-content row">

            </section>

        </section>

        <!-- TAGS -->
        @if(B2bArticle::tags_count($roba_id)>1)

        {{ B2bArticle::get_tags($roba_id) }}
        @endif
                <!-- RELATED PRODUCTS -->

        <h2 class="h2-margin"><span class="heading-background">Srodni proizvodi</span></h2>

        <section class="related-products product-slider">

            {{ B2bArticle::get_relatedB2b($roba_id) }}

        </section>

    </section>
@endsection

@section('footer')
    <section class="popup info-confirm-popup info-popup">

        <div class="popup-wrapper">

            <section class="popup-inner">



            </section>

        </div>

    </section>
    <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/slick.min.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/jquery.elevateZoom-3.0.8.min.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/jquery.fancybox.pack.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript" ></script>

    <script>
        $(document).ready(function () {
           var id = {{$roba_id}};
           // $('.add-amount-less').click(function (){
           //    var quantity = $('.add-amount').val();
           //    if(quantity <= 1){
           //        $('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");

           //        $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
           //    }
           //    else {
           //        $('.add-amount').val(quantity-1);
           //    }
           // });

           //  $('.add-amount-more').click(function (){
           //      var quantity = $('.add-amount').val();
           //      var max = $(this).data('max-quantity');
           //      if(quantity == max){
           //          $('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");

           //          $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
           //      }
           //      else {
           //          $('.add-amount').val(Number(quantity)+1);
           //      }
           //  });

           //  $('.add-amount').change(function() {
           //     var quantity = $(this).val();
           //     var max = $(this).data('max-quantity');
           //      if(quantity <= 1){
           //          $('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");

           //          $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
           //          $(this).val(1);
           //      }
           //      else if(quantity >= max){
           //          $('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");

           //          $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
           //          $(this).val(max);
           //      }

           //  });
            $('.add-to-cart-btn').click(function (){
                var quantity = $('.add-amount').val();
                var max = $('.add-amount').data('max-quantity');
                if(quantity < 1){
                    $('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");

                    $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
                    $(this).val(1);
                }
                else if(quantity > max){
                    $('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");

                    $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
                    $(this).val(max);
                }
                else {
                    var _this = $(this);
                    $.ajax({

                        type: "POST",
                        url:'{{route('b2b.cart_add')}}',
                        cache: false,
                        data:{roba_id:id, status:2, quantity:quantity},
                        success:function(res){
                            $('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
                            $('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
                            $('#broj_cart').text(res.countItems);
                            $('#header-cart-content').html(res.cartContent);
                            $('.add-amount').data('max-quantity', res.cartAvailable);
                            $('.add-amount-more').data('max-quantity', res.cartAvailable);
                            location.reload();

                        }

                    });
                }

            });
            $('.addCart').click(function(){
               var roba_id = $(this).data('product-id');
               var max = $(this).data('max-quantity');
                if(max <=0 ){
                    $('.info-popup').fadeIn("fast").delay(3000).fadeOut("fast");

                    $('.info-popup .popup-inner').html("<p class='p-info'>Za dati arikal dodali ste maksimalnu količinu u korpu.</p>");
                }
                else {
                    var _this = $(this);
                    $.ajax({

                        type: "POST",
                        url:'{{route('b2b.cart_add')}}',
                        cache: false,
                        data:{roba_id:roba_id, status:2, quantity:1},
                        success:function(res){
                            $('.info-popup').fadeIn("fast").delay(3000).fadeOut("fast");

                            $('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
                            location.reload();

                        }

                    });
                }
            });

        });
    </script>


@endsection