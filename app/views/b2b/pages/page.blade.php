@extends('b2b.templates.main')
@section('content') 
@if($count > 0)
<div class="main-content">
    <div class="row">  
        <div class="col-md-12 col-sm-12 col-xs-12 cont-replace"> 
			<div class="row product-list-options">  <!-- ================= PRODUCT INFO OPTIONS ===================== -->
			    <div class="col-md-7 col-sm-8 col-xs-12 sm-text-center"> 
			        <span>Ukupno: {{ $count }} </span>
			        @if(Options::product_number()==1)
					<div class="dropdown inline-block">	 
						 <button class="btn currency-btn dropdown-toggle" type="button" data-toggle="dropdown">	
						 	@if(Session::has('limit'))
							{{Session::get('limit')}}
							@else
							20
							@endif
			    			<span class="caret"></span>
			    		</button>
						<ul class="dropdown-menu currency-list">			
							<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/20" rel="nofollow">20</a></li>
							<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/30" rel="nofollow">30</a></li>
							<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/50" rel="nofollow">50</a></li>			
						</ul>			 
					</div>
					@endif
				    @if(B2bOptions::product_sort()==1)
				        <div class="dropdown"> 
				            <button class="btn currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
				                {{B2bCommon::get_sort()}} 
				                <span class="caret"></span>
				            </button>
				            <ul class="dropdown-menu currency-list">
				                @if(B2bOptions::web_options(207) == 0)
				                <li><a href="{{route('b2b.ordering',['price_asc'])}}">Cena - Rastuće</a></li>
				                <li><a href="{{route('b2b.ordering',['price_desc'])}}">Cena - Opadajuće</a></li>
				                @else
				                <li><a href="{{route('b2b.ordering',['price_desc'])}}">Cena - Opadajuće</a></li>
				                <li><a href="{{route('b2b.ordering',['price_asc'])}}">Cena - Rastuće</a></li>
				                @endif
				                <li><a href="{{route('b2b.ordering',['news'])}}">Najnovije</a></li>
				                <li><a href="{{route('b2b.ordering',['name'])}}">Prema nazivu</a></li>
				            </ul>
				        </div>
				    @endif 
			       
			        @if(B2bArticle::b2bproduct_view()==1)
			            <div class="view-buttons"> 
			                <a href="{{ B2bOptions::base_url() }}b2b/prikaz/table">
			                    <span class="{{(Session::has('b2b_prikaz') AND Session::get('b2b_prikaz') == 'table' OR !Session::has('b2b_prikaz')) ? 'active' : '' }}">
			                         <i class="fas fa-bars"></i> 
			                    </span>
			                </a>
			                <a href="{{ B2bOptions::base_url() }}b2b/prikaz/grid">
			                    <span class="{{(Session::has('b2b_prikaz') AND Session::get('b2b_prikaz') == 'grid') ? 'active' : '' }}">
			                        <i class="fas fa-th"></i>
			                    </span>
			                </a>       
			            </div>
			        @endif  
			    </div>
			    
				  <!-- ========= PAGINATION ==============-->
				</div>
				<div class="text-center">
					{{ Paginator::make($article, $count, $limit)->links() }}
				</div> 
	            <div class="row">       <!-- ============ PRODUCT CONTENT ============== -->
	                @if(Session::has('b2b_prikaz'))
	                    @if(Session::get('b2b_prikaz') == 'grid')
	                     @foreach($query_products as $row)
	                        @include('b2b.partials/product_on_grid')
	                     @endforeach
	   
	                    @elseif(Session::get('b2b_prikaz') == 'table')
		                    @foreach($query_products as $row)
		                        @include('b2b.partials/product_on_table')
		                    @endforeach 
	                    @endif   
	                @else
	                @foreach($query_products as $row)
	                     @include('b2b.partials/product_on_grid')       
	                @endforeach
	                @endif

	            </div>
	<!--========== PAGINATION DOWN ========= -->
	            <div class="row text-center"> 
	            	{{ Paginator::make($article, $count, $limit)->links() }}
	            </div> 
		</div>
	</div>
</div>
@else	
	<!-- page.blade -->
	<div class="row"> 
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<br> 
	        {{ $web_content}}
	    </div>
	</div> 		 

	<!-- page.blade end-->  
	
	<input type="hidden" id="base_url" value="{{B2bOptions::base_url()}}" />
@endif
@endsection 