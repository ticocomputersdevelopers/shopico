<!DOCTYPE html>
<html><head>
<meta charset="UTF-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>PDF</title>
    <style>
    *  {
        margin: 0;
        padding: 0;
        font-family: DejaVu Sans;
        box-sizing: border-box;
        font-size: 14px;
    }
    body { position: relative; margin-bottom: 190px !important; }
    .col-6 {
        width: 48%;
        display: inline-block;
        margin-right: 1%;
    }
    .fiskalni-racun-pdf {
        text-align: center; 
        margin: 0 30px;
        padding: 0 25px;
    }
    ul { list-style: none; }

    .fiskalni-racun-main li { 
        display: inline-block;
        max-width: 100%; 
        /*padding-bottom: .3rem;*/
        margin: 0;
        text-align: left; 
    }
    span { display: block; }
    .fakturisano {
        /* padding-top: 100px; */
        position: absolute;
        bottom: -160px;
        left: 55px;
        width: 700px;  
    }
    .main-wrapper { padding-bottom: 10px; }

    .text-left { text-align: left; }
    .text-right { text-align: right; }
    .text-bold { font-weight: bold; }
    .text-uppercase { text-transform: uppercase; }
    .text-center { text-align: center; }

    .obuka-prodaja h1 { font-size: 18px; }
    .obuka-prodaja table td { font-size: 12px; padding: 0; }

    small { font-size: 11px; }
    /* .testDiv { margin-top: -150px; } */

    hr {
        background-color: #222; 
        border: 0 none;
        color: #000;
        height: 1px;
    }
    hr.light {
        background-color: #eee;
        border: 0 none;
        color: #eee;
        height: 1px;
    }
    hr.invisible { background: transparent; }
    .border-top-black { border-top: 1px solid #000; }

    .border-btm-list {
        border-bottom: 1px solid #333;
    }
    .relative { position: relative; }
    .right-ordered-list li { position: relative; padding: 0; }

    .right-ordered-list span { display: inline-block; text-align: right; }

    .right-ordered-list li { margin: 0px 0;}

   /* .bottom-w-100 { width: 100%; position: absolute; bottom: 10%; }*/
    .qr-code { height: 180px; }
    tr  th { width: 138px; text-align: center; }
    .table-stripped tr th { 
        width: auto;
        background: #e7e7e7;
        border: 1px solid #5e5e5e;
        padding: 5px;
     }
     .fiscal-table tr th {
        width: auto;
        background: #e7e7e7;
        border: 1px solid #5e5e5e;
        padding: 3px;
        font-size: 13px;
     }

    tr th:first-child { text-align: left; }
    tr th:last-child { text-align: right; }
    table { border-collapse: collapse; width: 100%; }

    .border-bottom-td td { border-bottom: 1px solid #000; }
    .bottom-w-100 tbody .border-bottom-td { text-align: center; }
    .bottom-w-100 tbody .border-bottom-td td:last-child { text-align: right; }
    .stope tbody tr td:first-child { text-align: left; }
    .stope tr th { text-align: left; }
    .col-3 { width: 45%; display: inline-block; text-align: left; padding: 0; margin: 0; line-height: 1 }
    .col-9 { width: 50%; text-align: right; display: inline-block; padding: 0; margin: 0; line-height: 1 }
    .col-3-lg { width: 31%; display: inline-block; text-align: left; padding: 0; margin: 0; line-height: 1; }
    .col-9-lg { width: 66%; text-align: center; display: inline-block; padding: 0; margin: 0; line-height: 1 }


    .li-d-block li {
        display:block;
    }
    .py-1 { padding: 10px; }

    .col-9-b { display: block; margin: 0; padding: 0; }
    
    .right-div > div { 
        text-align: right;
        font-size: 12px !important;
    }
    .not-fiskal { 
        border-bottom: 1px solid #000;
        margin: 0 55px;
    }
    .not-fiskal > div {
        position: absolute; 
        left: 40%;  
        background: #fff;
        top: -13px;
        padding: 0 10px;
        font-weight: 600;
    } 
    .right-div span { display: block; }
    .table-stripped, .table-stripped th, .table-stripped td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    .fiscal-table th, .fiscal-table td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    .table-stripped th, .table-stripped td { padding: 5px !important; }
    .table-stripped th, .fiscal-table th { font-weight: normal; }
    .fiscal-table td { font-weight: normal; text-align: right; }

   </style>

   </head>


   <body style="margin-top: 30px">  
    <header style="margin-top: -30px;">
        <div class="text-center">
            <img src="{{Options::company_logo()}}" alt="logo" style="width: 150px;">
        </div>
    </header>

   <footer style="position: fixed; bottom: 180px; width: 700px; left: 55px;">
    <div class="">
    <div class="potpis">
        <div style="margin-bottom:10px;"> 
            <table>
                <tr>
                    <td style="width:20%;"><img style="max-width: 180px; max-height: 70px;" src="{{ AdminSupport::potpis() }}"><span style="border-top: 1px solid #000;" class="robu_izdao text-center">{{ AdminLanguage::transAdmin('Fakturisao') }}</span></td>                            
                    <td style="width: 44%;"></td>
                    <td style="width: 18%;"></td>
                </tr>
            </table>
        </div> 

        <div> 
            <div style="width: 100%; height: 1px; background: #000;"></div>
            <table class="signature">
                <tr>
                    <td class="text-left" style="width: 40%; font-size:11px">{{ Options::company_adress() }}</td>
                    <td class="text-center" style="width: 24%; font-size:11px">{{ Options::company_city() }}</td>
                    <td class="text-right" style="width: 18%; font-size:11px border-bottom: 1px solid #000;">ŠD:</td>
                </tr>

                <tr>
                    <td class="text-left" style="width: 40%; font-size:11px">Tel: {{ Options::company_phone() }}</td>
                    <td class="text-right" style="width: 24%; font-size:11px"></td>
                    <td class="text-right" style="width: 18%; font-size:11px">MBR: {{ Options::company_maticni() }}</td>
                </tr>

                <tr>
                    <td class="text-left" style="width: 40%; font-size:11px;">e-mail: {{ Options::company_email() }}</td>
                    <td class="text-center" style="width: 40%; font-size:11px">Tekući račun: {{ Options::company_ziro() }}</td>
                    <td class="text-right" style="width: 20%; font-size:11px">PIB: {{ Options::company_pib() }}</td>
                </tr>
            </table>
            <div style="width: 100%; height: 1px; background: #000;"></div>

        </div>
    </div> 
        </div>
       </footer>

    <div class="main-wrapper">

        <div class="relative not-fiskal">
            @if($racun->tip_racuna != 'Normal') 
            <div style="position: absolute; left: 33%;"> OVO NIJE FISKALNI RAČUN </div>
            @else
            <div> FISKALNI RAČUN </div>
            @endif
        </div>

        <div class="fiskalni-racun-pdf">
            <div class="fiskalni-racun-main">
               
                <div class="col-6">
                    <ul>
                        <!-- <li>{{ Options::company_phone() }}</li> -->
                        <li style="font-size: 12px;">PIB: {{ Options::company_pib() }}</li>
                        <hr class="invisible">

                        <li style="font-size: 12px;">{{ Options::company_contact_person() }}</li>
                        <hr class="invisible">

                        <li style="font-size: 12px;"> {{ AdminFiskalizacija::cirilicToLatin($racun->naziv_lokacije) }}</li>
                        <hr class="invisible">

                        <li style="font-size: 12px;"> {{ AdminFiskalizacija::cirilicToLatin($racun->adresa) }}</li>
                        <hr class="invisible">

                        <li style="font-size: 12px;">Kasir: {{ $racun->kasir }} </li>
                        <hr class="invisible">
 
                        <li style="font-size: 12px;"> {{ AdminFiskalizacija::cirilicToLatin($racun->okrug) }}</li>
                        <hr class="invisible">   

                        <li style="font-size: 12px;">ID kupca: {{ $racun->kupac_id }}</li>
                        <hr class="invisible">

                        <li style="font-size: 12px;">Esir broj: {{ $racun->esir }}</li>
                        <hr class="invisible">

                        <li style="font-size: 12px;">Datum izdavanja računa: {{ date('d.m.Y.',strtotime($racun->datum_racuna)) }}</li>
                        <hr class="invisible">

                        <li style="font-size: 12px;">Mesto izdavanja računa: {{ AdminFiskalizacija::cirilicToLatin($racun->okrug) }}</li>
                        <hr class="invisible">

                        <li style="font-size: 12px;">Tekući račun: {{ Options::company_ziro() }}</li>
                        <hr class="invisible">

                        <li style="font-size: 12px;">Valuta: RSD</li>
                        <hr class="invisible">


                        <!-- <li>PIB: {{ $racun->pib }}</li>
                        <hr class="invisible">

                        <li>Dobavljač: {{ $racun->poslovno_ime }}</li>
                        <hr class="invisible">

                        <li>Opciono polje kupca: {{ $racun->kupac_mesto_placanja_id }}</li>
                        <hr class="invisible">

                        <li>Esir vreme: {{ !empty($racun->datum_prometa) ? date('d.m.Y. H:i:s',strtotime($racun->datum_prometa)) : '' }}</li>
                        @if(!empty($racun->referentni_broj_dokumenta))
                        <hr class="invisible">

                        <li>Ref. broj: {{ $racun->referentni_broj_dokumenta }}</li>
                        <hr class="invisible">

                        <li>Ref. vreme: {{ date('d.m.Y. H:i:s',strtotime($racun->referentni_dokument_datum)) }}</li>
                        @endif
                        <hr class="invisible"> -->
                    </ul>
                </div>

                <?php $web_kupac = DB::table('web_kupac')->where('web_kupac_id',AdminFiskalizacija::getKupacID($racun->web_b2c_narudzbina_id))->first(); ?>

                <div class="col-6 right-div">
                    <!-- HARD CODED -->
        
                    <div class="text-right">Kupac: <span class="text-bold">{{ $web_kupac->naziv }}</span></div>

                    <div class="text-right"><span>{{ $web_kupac->ime }} {{ $web_kupac->prezime }}</span></div> 

                    <div class="text-right">{{ $web_kupac->adresa }}</div>
                    <div class="text-right">{{ $web_kupac->mesto }}</div>
                    @if(!empty($web_kupac->pib))
                    <div class="text-right">PIB: {{ $web_kupac->pib }}</div>
                    @endif
                    @if(!empty($web_kupac->maticni_br))
                    <div class="text-right">Matični broj: {{ $web_kupac->maticni_br }}</div>
                    @endif

                    <div class="nacin-placanja" style="margin: 0; padding: 0;">
                        Način plaćanja:
                        @foreach($placanja as $placanje)
                            <small>{{ AdminFiskalizacija::getPlacanjeNaziv($placanje->racun_vrsta_placanja_naziv_id) }}</small>
                            <!-- <span>: {{ str_replace(".",",",$placanje->iznos) }}</span> -->
                        @endforeach
                    </div>
                </div>
 
                </div>
        
            <br><br> 

            <!-- Obuka prodaja --> 
            <div class="obuka-prodaja"> 
                <h1 class="text-uppercase text-left">{{ AdminFiskalizacija::mappedVrstaRacuna($racun->tip_racuna,$racun->vrsta_transakcije) }}</h1>
                <br>

            <table>
                <tr>
                    <td class="text-left" style="width: 40%; font-size:11px">datum prometa i dobara i usluga: {{ date('d.m.Y.',strtotime($racun->datum_racuna)) }}</td>
                    <td class="text-right" style="width: 24%; font-size:11px">mesto prometa: {{ AdminFiskalizacija::cirilicToLatin($racun->okrug) }}</td>
                    <td class="text-right" style="width: 18%; font-size:11px">magacin: 1</td>
                    <td class="text-right" style="width: 18%; font-size:11px">šifra kupca: </td>
                </tr>
            </table>

            <table class="table-stripped">
                <tr>  
                    <th style="width: 5% !important;">RB</th> 
                    @if(AdminOptions::sifra_view()==1)
                        <th style="width:10% !important;">Roba_id</th>
                    @elseif(AdminOptions::sifra_view()==2)
                        <th style="width:10% !important;">Šifra IS</th>
                    @elseif(AdminOptions::sifra_view()==3)
                        <th style="width:10% !important;">SKU</th>
                    @elseif(AdminOptions::sifra_view()==4)
                        <th style="width:10% !important;">Šifra</th>
                    @endif
                    <!-- <th style="width:10% !important;">Šifra</th> -->
                    <th style="width: 30% !important;">Naziv</th>
                    <th style="width: 5% !important;">JM</th>
                    <th>Kol.</th>
                    <th >Cena</th>
                    <th>Iznos</th>
                </tr> 
                <?php $rbr = 0;
                 ?>
                @foreach($stavke as $stavka)
                <?php $rbr =  $rbr + 1;
                      if($stavka->etikete == 'А') {
                        $stavka_porez = 0;
                      } elseif ($stavka->etikete == 'Ђ') {
                        $stavka_porez = 20;
                      } elseif($stavka->etikete == 'Е') {
                        $stavka_porez = 10;
                      } elseif($stavka->etikete == 'Г') {
                        $stavka_porez = 0;
                      }
                ?>
                <tr class="center-th">
                    <td class="text-center" style="border-bottom: 2px solid #ccc !important;">{{ $rbr }}</td>
                    @if(AdminOptions::sifra_view()==1)
                        <td>{{ $stavka->roba_id }}</td>
                    @elseif(AdminOptions::sifra_view()==2)
                        <td>{{ AdminArticles::find($stavka->roba_id, 'sifra_is') }}</td>
                    @elseif(AdminOptions::sifra_view()==3)
                        <td>{{ AdminArticles::find($stavka->roba_id, 'sku') }}</td>
                    @elseif(AdminOptions::sifra_view()==4)
                        <td>{{ AdminArticles::find($stavka->roba_id, 'sifra_d') }}</td>
                    @endif   
                    
                    <td>{{ $stavka->naziv_stavke }} ({{$stavka->etikete}})</td>
                    <td class="text-center">{{ explode("/",$stavka->naziv_stavke)[sizeof(explode("/",$stavka->naziv_stavke))-1] }}</td>
                    <td class="text-right">{{ $stavka->kolicina }}</td>
                    <td class="text-right">{{ number_format($stavka->pcena,2, '.', ',') }}</td>
                    <td class="text-right">{{ number_format($stavka->uk_cena,2, '.', ',') }}</td>
                </tr>
                @endforeach
           
            


<!--                 <tr>
                    <td></td> 
                    <td colspan="8" style="font-style: italic; font-size: 10px; padding: 0 5px !important;"></td>
                </tr> -->

                </table>

            </div>
            <!-- Kraj obuka prodaja -->

            <!-- Nije fiskalni  -->
            <br>
            
            <div class="text-bold"> 
                @if($racun->tip_racuna != 'Normal') 
                    <div style="font-size: 18px;"> OVO NIJE FISKALNI RAČUN </div>
                @else
                    <div style="font-size: 18px;"> FISKALNI RAČUN </div>
                @endif
                <hr style="border-color: #000">

                <br>
 

                <table style="page-break-inside: avoid;">
                <div>
                    
                    <div class="py-1 col-3-lg">
                        <img src="data:image/gif;base64,{{$racun->qr_kod}}" class="qr-code" style="margin-top: -100px; transform: translateY(100px); height: 180px;">
                    </div>

                    <div class="col-9-lg">

                        <table class="fiscal-table">
                            <?php $ukupan_iznos_poreza = 0;
                                  $ukupan_iznos_osnovice = 0;
                             ?>
                            @foreach($porezi as $porez)  
                                @if($porez->iznos_poreske_stope != 0)
                                <?php $ukupan_iznos_osnovice = $ukupan_iznos_osnovice + $porez->iznos_poreza * 100 / $porez->iznos_poreske_stope; ?>                 
                                @endif
                                <?php $ukupan_iznos_poreza = $ukupan_iznos_poreza +  $porez->iznos_poreza; ?>
                            @endforeach
                            <tr>
                                <td colspan="3" style="border: 0">&nbsp;</td>
                            </tr>
    
                            <tr>
                                <td ><span class="text-bold">UKUPNO ZA UPLATU (RSD):</span> </td> 
                                <td colspan="2"><span class="text-bold">{{ number_format($racun->iznos,2, '.', ',') }}</span> </td>
                            </tr>
                            
                            @foreach($placanja as $placanje)
                            <tr>
                                <td>{{ AdminFiskalizacija::getPlacanjeNaziv($placanje->racun_vrsta_placanja_naziv_id) }}:</td> 
                                <td colspan="2">{{ number_format($placanje->iznos,2, '.', ',') }}</td>
                            </tr>
                            @endforeach

                        </table>
                        
    
                        <br>

                        <div class="pfr text-left" style="font-weight: normal;">
                            <span>PFR vreme: {{ date('d.m.Y. H:i:s',strtotime($racun->datum_racuna)) }}</span>
                            <span>PFR broj računa: {{ $racun->broj_dokumenta }}</span>
                            <span>Brojač računa: {{ $racun->brojac_racuna }}</span>
                        </div>

                    </div>

                </div>
                </table>

            </div>
         
            <!-- Kraj fiskalni -->
            <div class="relative not-fiskal" style="width: 100% !important; margin: 0 auto">
                @if($racun->tip_racuna != 'Normal') 
                    <div style="position: absolute; left: 33%;"> OVO NIJE FISKALNI RAČUN </div>
                @else
                    <div> KRAJ FISKALNOG RAČUNA </div>
                @endif 
            </div>

            <br>
            <!-- Napomena -->
            <div class="napomena text-left" style="font-size: 11px !important">
                <?php $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$racun->web_b2c_narudzbina_id)->first(); ?>
                <!-- <div>Napomena: {{ $web_b2c_narudzbina->napomena }}</div> -->
                <div>Napomena o poreskom oslobodjenju: Oslobodjeno placanja PDV-a po Cl.33 Zakona o PDV-u Republike Srbije.</div>
            </div>
            <!-- Kraj napomene -->

            <!-- Uslovi placanja -->
            <div class="uslovi-placanja text-left" style="font-size: 11px !important">
                <span>Uslovi plaćanja: {{ DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$web_b2c_narudzbina->web_nacin_placanja_id)->pluck('naziv') }}</span>
     
            </div>
            <!-- Kraj plaćanja -->

            <!-- Kraj wrappera -->
            </div>

      
            <div class="bottom-w-100" style="display: none">
                <ul>
                  <li><strong>Artikli:</strong></li>
                  <hr> 
                </ul>

                <table>
                    
                        <tr>
                            <th><strong>GTIN</strong></th>
                            <th><strong>Ime</strong></th>
                            <th><strong>Cena</strong></th>
                            <th><strong>Količina</strong></th>
                            <th><strong>Ukupna cena</strong></th> 
                        </tr>
                    
                    
                        @foreach($stavke as $stavka)
                            <tr class="border-bottom-td">
                                <td>{{ $stavka->gtin }}</td>
                                <td>{{ $stavka->naziv_stavke }} ({{$stavka->etikete}})</td>
                                <td>{{ number_format($stavka->pcena,2, ',', '') }}</td>
                                <td>{{ number_format($stavka->kolicina,3, ',', '') }}</td>
                                <td>{{ number_format($stavka->uk_cena,2, ',', '') }}</td>
                            </tr>
                        @endforeach
                    
                </table>

             

                <!-- <ul>
                  <li style="width: 40%;"><strong>GTIN</strong></li>
                  <li style="width: 12.5%;"><strong>Ime</strong></li>
                  <li style="width: 12.5%;"><strong>Cena</strong></li>
                  <li style="width: 12.5%;"><strong>Količina</strong></li>
                  <li style="width: 15.5%; padding-left: 2rem; text-align: right;"><strong>Ukupna cena</strong></li>
                  <hr style="margin: 0; padding: 0;"> 
                </ul>
                @foreach($stavke as $stavka)
                <ul class="border-btm-list">
                  <li style="width: 27%; text-align: center; padding-left: 0rem; display: inline-block;">{{ $stavka->gtin }}</li>
                  <li style="width: 25%; text-align: left; padding-left: .2rem">{{ $stavka->naziv_stavke }} ({{$stavka->etikete}})</li>
                  <li style="width: 12.5%; text-align: left;"><strong>{{  number_format($stavka->pcena,2, ',', '') }}</strong></li>
                  <li style="width: 12.5%; text-align: center;"><strong>{{  number_format($stavka->kolicina,3, ',', '') }}</strong></li> 
                  <li style="width: 19.5%; text-align: right;"><strong>{{ number_format($stavka->uk_cena,2, ',', '') }}</strong></li>
                </ul>
                @endforeach

                <ul>
                  <li><strong>Poreske stope:</strong></li>
                  <hr>
                </ul>
                <ul style="padding: 0; margin: 0;">
                  <li style="width: 32%;"><strong>Oznaka</strong></li>
                  <li style="width: 21.6%;"><strong>Ime</strong></li>
                  <li style="width: 21.6%;"><strong>Stopa</strong></li>
                  <li style="width: 21.6%; text-align: right;"><strong>Porez</strong></li>
                </ul>
                <hr>
                @foreach($porezi as $porez)
                <ul style="padding:0; margin: 0;" class="border-btm-list">
                  <li style="width: 32%;">{{ $porez->poreska_oznaka }}</li>
                  <li style="width: 21.6%;">{{ $porez->naziv_kategorije }}</li>
                  <li style="width: 21.6%;">{{ number_format($porez->iznos_poreske_stope,2,',','') }} %</li>
                  <li style="width: 21.6%; text-align: right;">{{ number_format($porez->iznos_poreza,2,',','') }}</li>
                </ul>
                @endforeach
                <hr> -->

                <br><br>

                <table class="stope">
                    
                        <tr>
                          <th>Poreske stope:</th>
                        </tr> 
                   
                        @foreach($porezi as $porez)
                        <tr class="border-bottom-td">
                            <td>{{ $porez->poreska_oznaka }}</td>
                            <td>{{ $porez->naziv_kategorije }}</td>
                            <td>{{ number_format($porez->iznos_poreske_stope,2,',','') }} %</td>
                            <td>{{ number_format($porez->iznos_poreza,2,',','') }}</td>
                        </tr>
                        @endforeach
                    
                </table>

            </div>

        </div>
    </div>

   </body></html>