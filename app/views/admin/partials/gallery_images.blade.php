
@foreach($images as $image)
<div class="column large-2 medium-3 small-12 no-padd">
	<div class="padding-h-8 padding-v-8 margin-bottom col-slika">
		<div class="modal-image-add flex"><img class="JSSelectGalleryImage pointer" src="{{ AdminOptions::base_url() }}images/upload_image/{{ $image }}" data-id="{{ $image }}"></div>
		<span class="img-desc" title="{{ $image }}">{{ $image }}</span>
		<a class="btn btn-danger JSGalleryImageDelete" data-id="{{ $image }}"> {{ AdminLanguage::transAdmin('Obriši') }} </a>
	</div>
</div>
@endforeach