<div class="row"> 
	<div class="napomena">
		<p>{{ AdminLanguage::transAdmin('Kod plaćanja - poziv na broj') }}: {{AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta')}}</p>
		@if(AdminOptions::web_options(311)==0)
		<p><strong>Napomena o poreskom oslobađanju: "{{AdminOptions::company_name()}}" nije u sistemu pdv-a </strong></p>
		@else
		<p><strong>Napomena o poreskom oslobađanju: Nema </strong></p>
		@endif
		<p>{{substr(AdminNarudzbine::find($web_b2c_narudzbina_id,'napomena'),0,220)}}</p>
	</div>
</div>

<div class="row"> 
	<table class="signature">
		<tr>
			<td>Predračun izdao: Marijan Kitanović</td>
			<td class=""></td>
			<td class="text-right"><span class="robu_izdao">{{ AdminLanguage::transAdmin('Predračun izdao') }}</span><img style="max-width: 220px; max-height: 90px;" src="{{ AdminSupport::potpis() }}"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style='width: 60%;'>
				<p style="border-top: 1px solid #666; font-size:12px;"> {{ AdminLanguage::transAdmin('Predračun je izrađen na računaru i punovažan je bez pečata i potipisa') }}</p>
			</td>
			<td class=""></td>
		</tr>
		@if(AdminOptions::web_options(311)==0)
		<tr>
			<td>Oslobođeno plaćanja PDV-a po čl. 33 Zakona o PDV-u R.S.</td>
		</tr>
		@endif
		<tr>
			<td>U skladu sa čl. 9 Zakona o računovodstvu, račun sadrži identifikacionu oznaku odgovornog lica za izdavanje dokumenta i kao takva važi bez potpisa i pečata.</td>
		</tr>
	</table>
</div>