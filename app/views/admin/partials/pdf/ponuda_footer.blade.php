<div class="row"> 
	<div class="napomena">
		<p>{{ AdminLanguage::transAdmin('Kod placanja - poziv na broj') }}: {{AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta')}}</p>
		@if(AdminOptions::web_options(311)==0)
		<p><strong>Napomena o poreskom oslobađanju: "{{AdminOptions::company_name()}}" nije u sistemu pdv-a </strong></p>
		@else
		<p><strong>Napomena o poreskom oslobađanju: Nema </strong></p>
		@endif
		<p>{{substr(AdminNarudzbine::find($web_b2c_narudzbina_id,'napomena'),0,220)}}</p>
		<br><br>
	</div>
</div>

<div class="row"> 
	<table class="signature">
		<tr>
			<td class="">
				<p><strong>{{ AdminLanguage::transAdmin('Opcija ponude') }}:</strong></p>
				<p>_______________________________</p>				
			</td>
			<td class="text-right"><span class="robu_izdao">{{ AdminLanguage::transAdmin('Odgovorno lice') }}</span>___________________________</td>
		</tr>
	</table>
</div>