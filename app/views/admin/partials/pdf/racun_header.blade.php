	<div class="row"> 
		<div class="logo col-3">
			<img src="{{Options::company_logo()}}" alt="logo">
		</div>
		<div class="col-4 company-info">
			<p class="comp-name">{{AdminOptions::company_name()}}</p>
			<p>{{AdminOptions::company_adress()}}, {{AdminOptions::company_mesto()}}</p>
			<p>{{ AdminLanguage::transAdmin('Telefon') }} {{AdminOptions::company_phone()}}, {{ AdminLanguage::transAdmin('Fax') }}: {{AdminOptions::company_fax()}}</p>
			<p>{{ AdminLanguage::transAdmin('PIB') }}: {{AdminOptions::company_pib()}}</p>
			<p>{{ AdminLanguage::transAdmin('Matični broj') }}: {{AdminOptions::company_maticni()}}</p>
		    <p>{{ AdminLanguage::transAdmin('E-mail') }}: {{AdminOptions::company_email()}}</p>
		</div>
		
		<div class="col-4 kupac-info">
			{{AdminSupport::narudzbina_kupac_pdf($web_b2c_narudzbina_id)}}
		</div>
 	</div>		

 	<div class="row"> 
		<p class="ziro">{{ AdminLanguage::transAdmin('Žiro racun') }}: {{AdminOptions::company_ziro()}}</p>
	 </div>

 	<div class="row">
		@if(DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('stornirano') == 1)
			<h4 class="racun-br">{{ AdminLanguage::transAdmin('Storno račun broj') }}: {{AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta')}}</h4>
		@else
			<h4 class="racun-br">{{ AdminLanguage::transAdmin('Račun broj') }}: {{AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta')}}</h4>
		@endif
	 </div>

	<div class="row"> 
		<table class="info-1">
			<thead>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Mesto i datum izdavanja računa') }}</td>
					<td>{{ AdminLanguage::transAdmin('Mesto i datum prometa dobara i usluga') }}</td>
					@if(Order::datum_narudzbine($web_b2c_narudzbina_id))
					<td>{{ AdminLanguage::transAdmin('Datum narudžbine') }}</td>
					@endif
					<td>{{ AdminLanguage::transAdmin('Valuta plaćanja') }}</td>
					<td>{{ AdminLanguage::transAdmin('Način isporuke') }}</td>
					<td>{{ AdminLanguage::transAdmin('Način plaćanja') }}</td>
				</tr>
			</thead>

			<tbody>
				<tr>						
					<td style="width: 165px;">{{AdminOptions::company_mesto()}}, {{AdminNarudzbine::formatDate(AdminNarudzbine::find($web_b2c_narudzbina_id,'datum_dokumenta'))}}</td>
					<td>{{AdminOptions::company_mesto()}}, {{AdminNarudzbine::formatDate(AdminNarudzbine::find($web_b2c_narudzbina_id,'datum_dokumenta'))}}</td>
					@if(Order::datum_narudzbine($web_b2c_narudzbina_id))
					<td>{{AdminNarudzbine::formatDate(AdminNarudzbine::find($web_b2c_narudzbina_id,'datum_narudzbine'),true)}}</td>
					@endif
					<td style="width: 40px;">{{ !empty(AdminNarudzbine::get_currency_payments($web_b2c_narudzbina_id)) ?AdminNarudzbine::get_currency_payments($web_b2c_narudzbina_id) : 'RSD'}}</td>
					<td>{{AdminCommon::n_i($web_b2c_narudzbina_id)}}</td>
					<td>{{AdminCommon::n_p($web_b2c_narudzbina_id)}}</td>
				</tr>
			</tbody>
		</table>
	</div>