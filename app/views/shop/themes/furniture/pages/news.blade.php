@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
 

	@foreach(All::getNews() as $row) 

	<div class="news relative sm-text-center">
	 
		<div class="row">
			<div class="col-md-3 col-sm-3 col-xs-12">
			 	<a class="overlink" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>

		        @if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))
		          
		        	<div class="bg-img" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></div>
		        
		        @else
		        
		        	<iframe width="560" height="315" src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   
		        
		        @endif
			</div>

		  	<div class="col-md-9 col-sm-9 col-xs-12">
				<h3 class="news-title">{{ $row->naslov }}</h3>

				<div class="news-desc">{{ All::shortNewDesc($row->tekst) }}</div>

				<div>
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" class="button inline-block relative z-index-1">{{ Language::trans('Pročitaj članak') }}</a>
				</div> 
			</div>
		</div> 
	</div>


	<!-- 	<div>
			{{ All::getNews()->links() }}
		</div>   -->

		@endforeach


@endsection