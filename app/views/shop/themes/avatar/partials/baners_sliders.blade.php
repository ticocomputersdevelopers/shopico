<!-- MAIN SLIDER -->  
<div class="slider-cont">    
    @if($firstSlider = Slider::getFirstSlider() AND count($slajderStavke = Slider::slajderStavke($firstSlider->slajder_id)) > 0)
    <div class="row">     
        <div class="col-md-9 col-sm-9 col-xs-12 no-padding"> 

            <div id="JSmain-slider" class="bw">
                @foreach($slajderStavke as $slajderStavka)

                    <div class="relative">

                        <a class="slider-link" href="{{ $slajderStavka->link }}"></a>

                        <div class="bg-img" style="background-image: url( '{{ Options::domain() }}{{ $slajderStavka->image_path }}' )"></div>

                        <div class="sliderText">

                            @if($slajderStavka->naslov != '')
                            <div>
                                <h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
                                    {{ $slajderStavka->naslov }}
                                </h2>
                            </div>
                            @endif

                            @if($slajderStavka->sadrzaj != '')
                            <div>
                                <div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
                                    {{ $slajderStavka->sadrzaj }}
                                </div>
                            </div>
                            @endif

                            @if($slajderStavka->naslov_dugme != '')
                            <div>
                                <a href="{{ $slajderStavka->link }}" class="slider-btn-link inline-block JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
                                    {{ $slajderStavka->naslov_dugme }}
                                </a>
                            </div>
                            @endif
                        </div>
                    </div>

                @endforeach
            </div>  
        </div>  

        <div class="col-md-3 col-sm-3 col-xs-12 no-padding JSsingle_ban hidden-sm hidden-xs">

            <!-- JS CONTENT -->

        </div>
    </div>
    @endif

    <!-- BANNERS -->
    @if($firstSlider = Slider::getFirstSlider('Baner') AND count($slajderStavke = Slider::slajderStavke($firstSlider->slajder_id)) > 0)
    <div class="row">
        @foreach($slajderStavke as $slajderStavka)
        <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
            <div class="relative banners"> 
                <a class="slider-link" href="{{ $slajderStavka->link }}"></a>
                                    
                <div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}')"></div>
            </div> 
        </div>
        @endforeach
    </div>  
    @endif
</div>

<script>
    $('.banners').each(function(i, ban){
        if(i == 0) {
            $(ban).parent().hide(function(){
                
                $('.JSsingle_ban').append( $(ban) );

            });
        }
    });
</script>