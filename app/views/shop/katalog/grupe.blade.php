<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>PDF</title>
<style>
* { 
	box-sizing: border-box;
 	padding: 0;
	margin: 0; 
	line-height: 1;	
} 
body {
	font-size: 14px;  
	font-family: DejaVu Sans; 
}
ul { list-style-type: none; }

table { 
	border-collapse: collapse; 
	margin-bottom: 15px; 
	width: 100%;
	table-layout: fixed;
}

table tr td, table tr th { 
	border: 1px solid #666;
	padding: 3px;  
	font-size: 13px;
} 
 
/*.row::after, .clearfix::after {content: ""; clear: both; display: table;}*/
[class*="col-"] { float: left; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
.inline-block { display: inline-block; } 
.relative { position: relative; }
.text-right { text-align: right; }
.pull-left { float: left; }
.pull-right { float: right; }

/************************/
img{
	max-width: 100%;
}
/************************/
.container{
	max-width: 90%;
	margin: auto;
	padding: 10px 0;
}
/************************/
.header{ 
	padding: 20px;  
	height: 120px; 
	overflow: hidden;
}
.header img {
	max-height: 100px;   
	margin: 25px 0;
} 
/************************/
.expires{
	margin: 3px 0;
	font-weight: 600;
}
/************************/
.li-group-1, .li-group-2, .li-group-3{ 
	padding: 10px 5px; 
	text-align: center;
	font-size: 16px;
	background-color: #4d85ff;
	border-top: 1px solid #000; 
}
.li-group-2{ 
	font-size: 90%;
	padding: 6px 5px;   
}
.li-group-3{ 
	padding: 4px 5px;  
	font-size: 90%; 
}
.li-group-2, .li-group-3{
	background-color: #6696ff;
	color: #fff;
}
/************************/
thead th{
	background-color: #e6e6e6;
}
thead th, .price, .pdv-price, .img-cont, .text-center{
	text-align: center;
} 
/************************/

.img-cont img{ max-width: 80px; }

/************************/
.price{
	width: 8%;
	font-weight: 600;
}
/************************/

.pdv-price, .img-cont{ width: 10%; } 

/************************/

.product-title{ width: 10%; padding: 3px 5px; }

/************************/ 
.descript{
	width: 40%;
	word-wrap: break-word;
}
.descript, .product-title{
	font-size: 11px; 
}
.descript ul li, .descript table td{
	 border: none;
	 border-top: 1px solid #000;
}
.descript h2{
	font-size: 20px;
}
.table-responsive{
	overflow-x: auto; 
}
@media screen and (max-width:768px) {
	.container{
		max-width: 100%;
		padding: 10px;
	}
	td, th{
		white-space: nowrap;
	}
}
/************************/
.footer { margin: 25px 0; }
</style>
</head>
<body>

	<div class="header">
		<div class="row"> 
			<div class="col-5">
				<img src="{{ B2bOptions::domain()}}{{B2bOptions::company_logo()}}" alt="logo">
			</div>
			<div class="col-7 text-right">
				<div>
					<p>Firma: {{B2bOptions::company_name()}}</p>
					<p>Adresa: {{B2bOptions::company_adress()}}</p>
					<p>Telefon: {{B2bOptions::company_phone()}}</p>
					<p>Fax: {{B2bOptions::company_fax()}}</p>
					<p>PIB: {{B2bOptions::company_pib()}}</p>
					<p>E-mail: {{B2bOptions::company_email()}}</p>
					<p>Web: {{B2bOptions::base_url()}}</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container relative"> 

		@if(!is_null($katalog->vazi_od))
		<div class="text-right expires">
			Važi od: {{ date("d.m.Y", strtotime($katalog->vazi_od)) }}. god
		</div>
		@endif

		<ul>	
    		@foreach(Groups::getLevelGroups(0) as $grupa1)
    			@if(Catalog::articlesCount($grupa1->grupa_pr_id,$katalog->katalog_id) > 0)
    				<li class="li-group-1">{{ $grupa1->grupa }}</li>
    				@if(Catalog::mainGroupArticlesCount($grupa1->grupa_pr_id,$katalog->katalog_id) > 0)
    				<li>
    				<div class="table-responsive"> 
					<table> 
						<thead> 
							<tr>
							@foreach($katalog_polja as $katalog_polje)
								<th>{{$katalog_polje->title}}</th>
							@endforeach
							</tr>
						</thead> 
				        <tbody> 
			        	@foreach(Catalog::groupArtcles($katalog->katalog_id,array($grupa1->grupa_pr_id)) as $articleMain1)
			        	<tr>
							@foreach($katalog_polja as $katalog_polje)
								@if($katalog_polje->naziv == 'web_cena') 
									<td class="price">{{ (Cart::cena(Product::get_price($articleMain1->roba_id)/1.2) )}}</td> 
								@elseif($katalog_polje->naziv == 'web_cena_pdv')
									<td class="pdv-price">{{ Cart::cena(Product::get_price($articleMain1->roba_id)) }}</td> 
								@elseif($katalog_polje->naziv == 'slika')
									<td class="img-cont"><img src="{{ B2bOptions::domain() }}{{ Product::web_slika($articleMain1->roba_id) }}"/></td> 
								@elseif($katalog_polje->naziv == 'naziv')
									<td class="product-title">{{ Product::seo_title($articleMain1->roba_id) }}</td> 
								@elseif($katalog_polje->naziv == 'opis')
									<td class="descript">{{ $articleMain1->web_opis }}</td> 
								@elseif($katalog_polje->naziv == 'karakteristike')
									<td class="karakt">{{ Product::get_karakteristike_table($articleMain1->roba_id) }}</td> 
								@endif
							@endforeach
			        	</tr>
			            @endforeach
			            </tbody>
			        </table>
			    	</div>
			        </li>
    				@else

		    		@foreach(Groups::getLevelGroups($grupa1->grupa_pr_id) as $grupa2)
		    			@if(Catalog::articlesCount($grupa2->grupa_pr_id,$katalog->katalog_id) > 0)
		    				<li class="li-group-2">{{ $grupa2->grupa }}</li>
		    				@if(Catalog::mainGroupArticlesCount($grupa2->grupa_pr_id,$katalog->katalog_id) > 0)
		    				<li>
		    				<div class="table-responsive"> 
							<table>
								<thead> 
									<tr>
										@foreach($katalog_polja as $katalog_polje)
											<th>{{$katalog_polje->title}}</th>
										@endforeach
							        </tr>
						        </thead>
						        <tbody> 
					        	@foreach(Catalog::groupArtcles($katalog->katalog_id,array($grupa2->grupa_pr_id)) as $articleMain2)
					        	<tr>
									@foreach($katalog_polja as $katalog_polje)
										@if($katalog_polje->naziv == 'web_cena')
											<td class="price">{{ (Cart::cena(Product::get_price($articleMain2->roba_id)/1.2) )}}</td> 
										@elseif($katalog_polje->naziv == 'web_cena_pdv')
											<td class="pdv-price">{{ Cart::cena(Product::get_price($articleMain2->roba_id)) }}</td> 
										@elseif($katalog_polje->naziv == 'slika')
											<td class="img-cont"><img src="{{ B2bOptions::domain() }}{{ Product::web_slika($articleMain2->roba_id) }}"/></td> 
										@elseif($katalog_polje->naziv == 'naziv')
											<td class="product-title">{{ Product::seo_title($articleMain2->roba_id) }}</td> 
										@elseif($katalog_polje->naziv == 'opis')
											<td class="descript">{{ $articleMain2->web_opis }}</td> 
										@elseif($katalog_polje->naziv == 'karakteristike')
											<td class="karakt">{{ Product::get_karakteristike_table($articleMain2->roba_id) }}</td> 
										@endif
									@endforeach
					        	</tr>
					            @endforeach
					            </tbody>
					        </table>
					    	</div>
					        </li>

		    				@else

				    		@foreach(Groups::getLevelGroups($grupa2->grupa_pr_id) as $grupa3)
				    			@if(Catalog::articlesCount($grupa3->grupa_pr_id,$katalog->katalog_id) > 0)
				    				<li class="li-group-3">{{ $grupa3->grupa }}</li>
				    				@if(Catalog::mainGroupArticlesCount($grupa3->grupa_pr_id,$katalog->katalog_id) > 0)
				    				<li>
				    				<div class="table-responsive"> 
									<table>
										<thead>
											<tr>
												@foreach($katalog_polja as $katalog_polje)
													<th>{{$katalog_polje->title}}</th>
												@endforeach
									        </tr>
								        </thead>
								        <tbody> 
							        	@foreach(Catalog::groupArtcles($katalog->katalog_id,array($grupa3->grupa_pr_id)) as $articleMain3)
							        	<tr>
											@foreach($katalog_polja as $katalog_polje)
												@if($katalog_polje->naziv == 'web_cena')
													<td class="price">{{ (Cart::cena(Product::get_price($articleMain3->roba_id)/1.2) )}}</td> 
												@elseif($katalog_polje->naziv == 'web_cena_pdv')
													<td class="pdv-price">{{ Cart::cena(Product::get_price($articleMain3->roba_id)) }}</td> 
												@elseif($katalog_polje->naziv == 'slika')
													<td class="img-cont"><img src="{{ B2bOptions::domain() }}{{ Product::web_slika($articleMain3->roba_id) }}"/></td> 
												@elseif($katalog_polje->naziv == 'naziv')
													<td class="product-title">{{ Product::seo_title($articleMain3->roba_id) }}</td> 
												@elseif($katalog_polje->naziv == 'opis')
													<td class="descript">{{ $articleMain3->web_opis }}</td> 
												@elseif($katalog_polje->naziv == 'karakteristike')
													<td class="karakt">{{ Product::get_karakteristike_table($articleMain3->roba_id) }}</td> 
												@endif
											@endforeach
							        	</tr>
							            @endforeach
							            </tbody>
							        </table>
							    	</div>
							        </li>

				    				@else

						    		@foreach(Groups::getLevelGroups($grupa2->grupa_pr_id) as $grupa3)
						    			@if(Catalog::articlesCount($grupa3->grupa_pr_id,$katalog->katalog_id) > 0)
						    				<li class="li-group-3">{{ $grupa3->grupa }}</li>
						    				@if(Catalog::mainGroupArticlesCount($grupa3->grupa_pr_id,$katalog->katalog_id) > 0)
						    				<li>
						    				<div class="table-responsive"> 
											<table>
												<thead>
													<tr>
														@foreach($katalog_polja as $katalog_polje)
															<th>{{$katalog_polje->title}}</th>
														@endforeach
											        </tr>
										        </thead>
										        <tbody>
									        	@foreach(Catalog::groupArtcles($katalog->katalog_id,array($grupa3->grupa_pr_id)) as $articleMain3)
									        	<tr>
													@foreach($katalog_polja as $katalog_polje)
														@if($katalog_polje->naziv == 'web_cena')
															<td class="price">{{ (Cart::cena(Product::get_price($articleMain3->roba_id)/1.2) )}}</td> 
														@elseif($katalog_polje->naziv == 'web_cena_pdv')
															<td class="pdv-price">{{ Cart::cena(Product::get_price($articleMain3->roba_id)) }}</td> 
														@elseif($katalog_polje->naziv == 'slika')
															<td class="img-cont"><img src="{{ B2bOptions::domain() }}{{ Product::web_slika($articleMain3->roba_id) }}"/></td> 
														@elseif($katalog_polje->naziv == 'naziv')
															<td class="product-title">{{ Product::seo_title($articleMain3->roba_id) }}</td> 
														@elseif($katalog_polje->naziv == 'opis')
															<td class="descript">{{ $articleMain3->web_opis }}</td> 
														@elseif($katalog_polje->naziv == 'karakteristike')
															<td class="karakt">{{ Product::get_karakteristike_table($articleMain3->roba_id) }}</td>
														@endif
													@endforeach
										        	</tr>
										            @endforeach
										            </tbody>
										        </table>
										        </div>
										        </li>
							    				@endif 
									            @endif
								            @endforeach 
									        @endif 
							            @endif
						            @endforeach 
						        @endif 
				            @endif
			            @endforeach 
			        @endif 
	            @endif
            @endforeach
	    </ul> 

	    <div class="footer text-center">
	    	<p>{{ B2bOptions::company_name() }} {{ B2bOptions::company_adress() }}</p>
	    	<p>{{ B2bOptions::company_email() }} &nbsp;&nbsp;&nbsp; {{ B2bOptions::base_url() }}</p>
	    </div>
	</div>  
</body>
</html>
