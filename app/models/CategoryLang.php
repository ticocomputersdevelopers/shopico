<?php

class CategoryLang extends Eloquent
{
    protected $table = 'grupa_pr_jezik';
    
    public $timestamps = false;

	protected $hidden = array(
		'grupa_pr_id',
        'jezik_id',
        'sablon_opis'
    	);

	public function getCategoryIdAttribute()
	{
	    return isset($this->attributes['grupa_pr_id']) ? $this->attributes['grupa_pr_id'] : null;
	}
    public function getLangIdAttribute()
    {
        return isset($this->attributes['jezik_id']) ? $this->attributes['jezik_id'] : null;
    }

	protected $appends = array(
    	'category_id',
        'lang_id'
    	);

    public function category(){
        return $this->belongsTo('Category','grupa_pr_id');
    }

    public function lang(){
        return $this->belongsTo('Languages','jezik_id');
    }
}