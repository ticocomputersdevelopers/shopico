<?php

class AdminFront {

	public static function saveSale($new_content)
	{
		if(strlen(strip_tags($new_content)) < 255){
			if(($langId=Language::lang_id()) == 1){
				DB::table('web_b2c_seo')->where('naziv_stranice','akcija')->update(array('seo_title'=>strip_tags($new_content)));
			}
			AdminTranslator::updateTranslator($langId,DB::table('web_b2c_seo')->where('naziv_stranice','akcija')->pluck('seo_title'),strip_tags($new_content));
		}
	}

	public static function saveBlogs($new_content)
	{
		if(strlen(strip_tags($new_content)) < 255){
			if(($langId=Language::lang_id()) == 1){
				DB::table('web_b2c_seo')->where('naziv_stranice','blog')->update(array('seo_title'=>strip_tags($new_content)));
			}
			AdminTranslator::updateTranslator($langId,DB::table('web_b2c_seo')->where('naziv_stranice','blog')->pluck('seo_title'),strip_tags($new_content));
		}
	}

	public static function saveAllArticles($new_content)
	{
		if(strlen(strip_tags($new_content)) < 255){
			if(($langId=Language::lang_id()) == 1){
				DB::table('web_b2c_seo')->where('naziv_stranice','pocetna')->update(array('seo_title'=>strip_tags($new_content)));
			}
			AdminTranslator::updateTranslator($langId,DB::table('web_b2c_seo')->where('naziv_stranice','pocetna')->pluck('seo_title'),strip_tags($new_content));
		}
	}

	public static function saveType($new_content,$type_id)
	{
		if(strlen(strip_tags($new_content)) < 200){
			if(($langId=Language::lang_id()) == 1){
				DB::table('tip_artikla')->where('tip_artikla_id',$type_id)->update(array('naziv'=>strip_tags($new_content)));
			}
			AdminTranslator::updateTranslator($langId,DB::table('tip_artikla')->where('tip_artikla_id',$type_id)->pluck('naziv'),strip_tags($new_content));
		}
	}

	public static function saveSlideTitle($new_content,$slider_item_id,$jezik_id)
	{
		if(strlen($new_content) < 100){

			$lang_data = DB::table('slajder_stavka_jezik')->where(array('slajder_stavka_id'=>$slider_item_id,'jezik_id'=>$jezik_id))->first();
			if(is_null($lang_data)){
				DB::table('slajder_stavka_jezik')->insert(array('slajder_stavka_id'=>$slider_item_id,'jezik_id'=>$jezik_id,'naslov'=>$new_content,'sadrzaj'=>'')); 
			}else{
				DB::table('slajder_stavka_jezik')->where(array('slajder_stavka_id'=>$slider_item_id,'jezik_id'=>$jezik_id))->update(array('naslov'=>$new_content)); 
			}
		}
	}

	public static function saveSlideContent($new_content,$slider_item_id,$jezik_id)
	{
		if(strlen(strip_tags($new_content)) < 260){
			$lang_data = DB::table('slajder_stavka_jezik')->where(array('slajder_stavka_id'=>$slider_item_id,'jezik_id'=>$jezik_id))->first();
			if(is_null($lang_data)){
				DB::table('slajder_stavka_jezik')->insert(array('slajder_stavka_id'=>$slider_item_id,'jezik_id'=>$jezik_id,'naslov'=>'','sadrzaj'=>$new_content)); 
			}else{
				DB::table('slajder_stavka_jezik')->where(array('slajder_stavka_id'=>$slider_item_id,'jezik_id'=>$jezik_id))->update(array('sadrzaj'=>$new_content)); 
			}
		}
	}

	public static function saveSlideButton($new_content,$slider_item_id,$jezik_id)
	{
		if(strlen(strip_tags($new_content)) < 260){
			$lang_data = DB::table('slajder_stavka_jezik')->where(array('slajder_stavka_id'=>$slider_item_id,'jezik_id'=>$jezik_id))->first();
			if(is_null($lang_data)){
				DB::table('slajder_stavka_jezik')->insert(array('slajder_stavka_id'=>$slider_item_id,'jezik_id'=>$jezik_id,'naslov'=>'','sadrzaj'=>'','naslov_dugme'=>$new_content)); 
			}else{
				DB::table('slajder_stavka_jezik')->where(array('slajder_stavka_id'=>$slider_item_id,'jezik_id'=>$jezik_id))->update(array('naslov_dugme'=>$new_content)); 
			}
		}
	}

	public static function saveFooterSectionLabel($new_content,$futer_sekcija_id,$jezik_id)
	{
		if(strlen(strip_tags($new_content)) < 250){
			$lang_data = DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id))->first();
			if(is_null($lang_data)){
				DB::table('futer_sekcija_jezik')->insert(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id, 'sadrzaj'=>'','naslov'=>strip_tags($new_content))); 
			}else{
				DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id))->update(array('naslov'=>strip_tags($new_content))); 
			}
		}
	}

	public static function saveFooterSectionContent($new_content,$futer_sekcija_id,$jezik_id)
	{
		$lang_data = DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id))->first();
		if(is_null($lang_data)){
			DB::table('futer_sekcija_jezik')->insert(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id, 'naslov'=>'','sadrzaj'=>$new_content)); 
		}else{
			DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id))->update(array('sadrzaj'=>$new_content)); 
		}
	}

	public static function saveNewslatterLabel($new_content,$jezik_id)
	{
		if(strlen(strip_tags($new_content)) < 250){
			$lang_data = DB::table('web_b2c_newslatter_description')->where(array('jezik_id'=>$jezik_id))->first();
			if(is_null($lang_data)){
				DB::table('web_b2c_newslatter_description')->insert(array('jezik_id'=>$jezik_id, 'sadrzaj'=>'','naslov'=>strip_tags($new_content))); 
			}else{
				DB::table('web_b2c_newslatter_description')->where(array('jezik_id'=>$jezik_id))->update(array('naslov'=>strip_tags($new_content))); 
			}
		}
	}

	public static function saveNewslatterContent($new_content,$jezik_id)
	{
		$lang_data = DB::table('web_b2c_newslatter_description')->where(array('jezik_id'=>$jezik_id))->first();
		if(is_null($lang_data)){
			DB::table('web_b2c_newslatter_description')->insert(array('jezik_id'=>$jezik_id, 'naslov'=>'','sadrzaj'=>$new_content)); 
		}else{
			DB::table('web_b2c_newslatter_description')->where(array('jezik_id'=>$jezik_id))->update(array('sadrzaj'=>$new_content)); 
		}
	}

	public static function saveFrontAdminLabel($content,$id)
	{
		if(strlen(strip_tags($content)) < 500){
			$first = DB::table('front_admin_labele')->where(array('front_admin_labele_id'=>$id))->first();
			if(($langId=Language::lang_id()) == 1){
				if(is_null($first)){
					DB::table('front_admin_labele')->insert(array('front_admin_labele_id'=>$id,'labela'=>strip_tags($content))); 
				}else{
					DB::table('front_admin_labele')->where(array('front_admin_labele_id'=>$id))->update(array('labela'=>strip_tags($content)));
				}
			}
			AdminTranslator::updateTranslator($langId,DB::table('front_admin_labele')->where(array('front_admin_labele_id'=>$id))->pluck('labela'),strip_tags($content));			
		}
	}
	public static function saveFrontAdminContent($content,$id,$jezik_id)
	{
		$lang_data = DB::table('front_admin_labele_jezik')->where(array('front_admin_labele_id'=>$id,'jezik_id'=>$jezik_id))->first();
		if(is_null($lang_data)){
			DB::table('front_admin_labele_jezik')->insert(array('front_admin_labele_id'=>$id,'jezik_id'=>$jezik_id,'sadrzaj'=>$content)); 
		}else{
			DB::table('front_admin_labele_jezik')->where(array('front_admin_labele_id'=>$id,'jezik_id'=>$jezik_id))->update(array('sadrzaj'=>$content)); 
		}
	}

	public static function saveFrontAdminPageSectionName($content,$id,$jezik_id)
	{
		if(strlen($content) < 255){
			$first = DB::table('sekcija_stranice')->where(array('sekcija_stranice_id'=>$id))->first();
			if(($langId=Language::lang_id()) == 1){
				if(is_null($first)){
					DB::table('sekcija_stranice')->insert(array('sekcija_stranice_id'=>$id,'naziv'=>$content)); 
				}else{
					DB::table('sekcija_stranice')->where(array('sekcija_stranice_id'=>$id))->update(array('naziv'=>$content));
				}
			}
			AdminTranslator::updateTranslator($langId,DB::table('sekcija_stranice')->where(array('sekcija_stranice_id'=>$id))->pluck('naziv'),$content);			
		}
	}

}