<?php
namespace IsMinimax;
use DB;
use Options;
use All;

class Article {

	public static function table_body($articles){
		//$kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');

		$result_arr = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $article) {
			if(isset($article['ItemId'])){
				$id_is = $article['ItemId'];
				$roba_id++;
				$sifra_k++;
			
				$sifra_is = isset($article['Code']) ? Support::convert($article['Code']) : '';
				$naziv = Support::convert($article['Title']);
				$web_opis = '';
				if($article['UnitOfMeasurement'] == 'KOM' || $article['UnitOfMeasurement'] == 'kom'){
				$jedinica_mere_id = 1;
				}elseif($article['UnitOfMeasurement'] == 'm2' || $article['UnitOfMeasurement'] == 'M2'){
				$jedinica_mere_id = 2;	
				}elseif($article['UnitOfMeasurement'] == 'kg' || $article['UnitOfMeasurement'] == 'KG'){
				$jedinica_mere_id = 3;
				}else{
				$jedinica_mere_id = -1;	
				}
				$proizvodjac_id = -1;
				$vrednost_tarifne_grupe = 20;
				$tarifna_grupa_id = 0;
				$grupa_pr_id = -1;
				$barkod = "NULL";
				// $grupa_pr_id = Support::getGrupaId('NOVI ARTIKLI');
				$kursValuta = 1;

				$racunska_cena_nc = isset($article['Price']) && is_numeric(floatval($article['Price'])) ? (floatval($article['Price']) * $kursValuta) : 0;
				$mpcena = 0;
				$web_cena = $mpcena;
				$racunska_cena_a = $racunska_cena_nc;
				$racunska_cena_end = $racunska_cena_nc;

				$roba_cene = Support::roba_cene($id_is);
				$stara_cena = !is_null($roba_cene) ? ($roba_cene->racunska_cena_end != $racunska_cena_end ? $roba_cene->racunska_cena_end : $roba_cene->stara_cena) : $racunska_cena_end;

				$marza = 0;
				$flag_aktivan = $racunska_cena_nc > 0 ? '1' : '0';
				// $flag_prikazi_u_cenovniku = isset($article->prikaz) && is_numeric(intval($article->prikaz)) && in_array(intval($article->prikaz),array(0,1)) ? strval($article->prikaz) : '0';
				$flag_cenovnik = '0';

				$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,".$flag_cenovnik.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_a).",".strval($racunska_cena_end).",".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,".strval($marza).",(NULL)::integer,'".$naziv."',".$flag_cenovnik.",NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,".$barkod.",".strval($stara_cena).",0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0,0,0,(NULL)::date,(NULL)::date,0.00,0.00,1)";
			}
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> '' AND t.flag_zakljucan='false'");	
		//insert
		DB::statement("INSERT INTO roba(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE (roba_temp.id_is)::varchar NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");


		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		//DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is NOT IN (SELECT id_is FROM ".$table_temp.")");
	}

}